import {createRouter, createWebHistory} from 'vue-router'
// import HomeView from '../views/HomeView.vue'
import CallbackLogin from "@/components/CallbackLogin.vue";
import OAuthLogin from "@/components/OAuthLogin.vue";
import Gate from "@/components/Gate.vue";
import Staff from "@/components/Staff.vue";

const routes = [
    // {
    //     path: '/',
    //     name: 'home',
    //     component: HomeView,
    // },
    {
        path: '/dashboard',
        name: 'dashboard',
        meta: {layout: "defaultLayout"},
        component: () => import(/* webpackChunkName: "about" */ '../views/Main-dashboard.vue')
    },
    {
        path: '/',
        name: "callbackLogin",
        component: CallbackLogin,
    },
    // {path: '/oauth', name: "oauthLogin", component: OAuthLogin},
    // {path: '/gate', name: 'gate', component: Gate},
    {path: '/staff', name: 'staff', component: Staff}
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
});

export default router
