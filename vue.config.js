const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false,
  devServer: {
    client: {
      overlay: false,
    },
    proxy: {
      '^/ocs': {
        target: 'http://ted_back.develop',
        changeOrigin: true,
        secure: false,
        pathRewrite: { '^/ocs': '/ocs' },
      },
    },
  },
});
