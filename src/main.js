import {createApp} from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue-next';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue-next/dist/bootstrap-vue-next.css';
import "@/assets/css/main.css";
import './index.css';
import routes from "./router";
import "@mdi/font/css/materialdesignicons.css";
import "@fortawesome/fontawesome-free/css/all.css";

// Vuetify
import 'vuetify/styles'
import {createVuetify} from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import router from "./router";

const vuetify = createVuetify({
    components,
    directives,
})
const app = createApp(App).use(router);
app.use(BootstrapVue)
app.use(routes);
app.use(vuetify);
app.mount('#app');
